var subItemsDiv = document.querySelectorAll(".menu-bar-item-sub-items-div");
for (var i = 0; i < subItemsDiv.length; i++) {
	subItemsDiv[i].style.display = "none";
}

var subDeepness = 0;
var items = document.querySelectorAll(".menu-bar-item-div");
for (var i = 0; i < items.length; i++) {
	items[i].addEventListener("mouseenter", function () {
		var subsDiv = this.querySelector(".menu-bar-item-sub-items-div");
		if(subsDiv != null) {
			subsDiv.style.display = "inline-block";
			subsDiv.style.position = "absolute";

			var parentRect = subsDiv.parentNode.getBoundingClientRect();
			if(subDeepness > 0) {
				var parentWidth = parseInt(Math.abs(parentRect.right-parentRect.left));
				subsDiv.style.top = "0px";
				subsDiv.style.left = parentWidth+"px";
			}
			else {
				var parentHeight = parseInt(Math.abs(parentRect.bottom-parentRect.top));
				subsDiv.style.top = parentHeight+"px";
				subsDiv.style.left = "0px";
			}
			subDeepness++;
		}
	});
	items[i].addEventListener("mouseleave", function () {
		var subsDiv = this.querySelector(".menu-bar-item-sub-items-div");
		if(subsDiv != null) {
            subDeepness--;
			subsDiv.style.display = "none";
		}
	});
}
