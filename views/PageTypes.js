var {Section, SectionBlock, Dimensions} = require('./components/section/SectionTypes');
var {MenuBar, MenuBarItem} = require("./components/menu/MenuBarTypes");

/**
* @param {string} title
* @param {MenuBar} menuBar
* @param {Section[]} sections
* @param {Section} footerSection
*/
function Page (title, menuBar, sections, footerSection) {
	this.title = title;
	this.menuBar = menuBar;
	this.sections = sections;
	this.footerSection = footerSection;

	/**
	* @param {Section} section
	*/
	this.addSection = function (section) {
		if(this.sections == undefined) {
			this.sections = [];
		}
		this.sections.push(section);
	}
}

module.exports = {
	Section, SectionBlock, Dimensions,
	MenuBar, MenuBarItem,
	Page
}
