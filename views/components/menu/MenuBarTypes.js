/**
* @param {string} title
* @param {string} icon The icon link.
* @param {string} link The link to redirect to when user click on the item.
* @param {number} opacity [0.0-1.0]
* @param {boolean} selected Weather or not the item is selected. 
* @param {string} elementId
* @param {string} elementClass
*/
function MenuBarItem(title, icon, link, opacity, selected, elementId, elementClass) {
	this.elementId = elementId;
	this.elementClass = elementClass;

	this.title = title;
	this.icon = icon;
	this.link = link;
	this.opacity = opacity;
	this.selected = selected;
	this.subItems = undefined;

	/**
	* @param {MenuBarItem} item
	*/
	this.addSubItem = function (item) {
		if(this.subItems == undefined) {
			this.subItems = [];
		}
		this.subItems.push(item);
	}
}

/**
* @param {number} opacity [0.0-1.0]
* @param {string} elementId
* @param {string} elementClass
*/
function MenuBar(opacity, elementId, elementClass) {
	this.elementId = elementId;
	this.elementClass = elementClass;

	this.opacity = opacity;
	this.brandItem = undefined;
	this.itemMainZone = undefined;
	this.itemSecondaryZone = undefined;

	/**
	* @param {MenuBarItem} item
	*/
	this.setBrandItem = function (item) {
		this.brandItem = item;
	}
	/**
	* @param {MenuBarItem} item
	*/
	this.addItemToMainZone = function (item) {
		if(this.itemMainZone == undefined) {
			this.itemMainZone = [];
		}
		this.itemMainZone.push(item);
	}
	/**
	* @param {MenuBarItem} item
	*/
	this.addItemToSecondaryZone = function (item) {
		if(this.itemSecondaryZone == undefined) {
			this.itemSecondaryZone = [];
		}
		this.itemSecondaryZone.push(item);
	}

	this.selectMainItemByTitle = function (title) {
		for (var i = 0; i < this.itemMainZone.length; i++) {
			if(this.itemMainZone[i].title == title) {
				this.itemMainZone[i].selected = 1;
			}
		}
	}
	this.selectMainItemByElementId = function (id) {
		for (var i = 0; i < this.itemMainZone.length; i++) {
			if(this.itemMainZone[i].elementId == id) {
				this.itemMainZone[i].selected = 1;
			}
		}
	}
}

module.exports = {MenuBar, MenuBarItem};
