var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var nodeMailer = require('nodemailer');

var {
	Section, SectionBlock, Dimensions,
	MenuBar, MenuBarItem,
	Page,
	loadMenuBar, loadPage
} = require('./views/PageLoader');

app.set('view engine', 'ejs');
app.set('views', './views');
app.use("/provides", express.static("./provides"));

app.use("*", function (req, res, next) {
	var htmlpath = "contents"+req.baseUrl+".html";
	console.log(htmlpath);
	loadPage(htmlpath, "contents/menuBar.html",
		function (page) {
			console.log("Send Page");
			res.render('DefaultPage', {page});
    		res.gumError = 0;
		},
		function () {
			console.log("Send serveur indisponible");
			res.gumError = 1;
			next();
		});
});

app.use(function (req, res, next) {
	console.log("Receive unknown request : " + req.originalUrl);

	console.log(req.baseUrl);

	if(res.gumError == undefined || res.gumError > 0) {
		loadPage("contents/error.html", "contents/menuBar.html",
			function (page) {
				if (res.gumError == undefined) {
					page.sections[0].title = "Page introuvable";
					res.status(404);
					res.render('DefaultPage', {page});
				}
				else if(res.gumError == 1) {
					// Not found
					page.sections[0].title = "Serveur indisponible";
					res.status(500);
					res.render('DefaultPage', {page});
				}
			},
			function () {
				console.log("Send faile");
				res.send("Le serveur ne peut pas acceder a cette resource");
			});
	}

});

let port = process.env.PORT;
if(port == null || port == "") {
    port = 8000;
}
console.log("server is starting on port "+port+" ...");
app.listen(port);
console.log("Server started");
