var fs = require('fs');
var parser = require('node-html-parser');

var {
	Section, SectionBlock, Dimensions,
	MenuBar, MenuBarItem,
	Page
} = require('./PageTypes');

function getNodeContentBySelector(block, selector, defaultReturn) {
	if(block == null || block == undefined) {
		return defaultReturn;
	}

    var node = block.querySelector(selector);
    if(node == null || node.innerHTML == "") {
        return defaultReturn;
    }
    else {
        return node.innerHTML;
    }
}

function createMenuBarItem(menuBarItemElem) {
	var item = new MenuBarItem(
		getNodeContentBySelector(menuBarItemElem, "> .title", undefined),
		getNodeContentBySelector(menuBarItemElem, "> .icon", undefined),
		getNodeContentBySelector(menuBarItemElem, "> .link", undefined),
		getNodeContentBySelector(menuBarItemElem, "> .opacity", undefined),
		getNodeContentBySelector(menuBarItemElem, "> .selected", undefined),
		getNodeContentBySelector(menuBarItemElem, "> .elementId", undefined),
		getNodeContentBySelector(menuBarItemElem, "> .elementClass", undefined)
	);

	var subItemElems = menuBarItemElem.querySelectorAll("> .menuBarItem");
	for(var i=0; i<subItemElems.length; i++) {
		var elem = subItemElems[i];
		item.addSubItem(
			createMenuBarItem(elem));
	}

	return item;
}

function createMenuBar(menuBarElem) {
	var menuBar = new MenuBar(
		getNodeContentBySelector(menuBarElem, "> .opacity", undefined),
		getNodeContentBySelector(menuBarElem, "> .elementId", undefined),
		getNodeContentBySelector(menuBarElem, "> .elementClass", undefined)
	);

	var brandItem = menuBarElem.querySelector("> .brandItem");
	var itemMainZone = menuBarElem.querySelectorAll("> .itemMainZone");
	var itemSecondaryZone = menuBarElem.querySelectorAll("> .itemSecondaryZone");

	menuBar.setBrandItem(
		createMenuBarItem(brandItem));
	for(var i=0; i<itemMainZone.length; i++) {
		var elem = itemMainZone[i];
		menuBar.addItemToMainZone(
			createMenuBarItem(elem));
	}
	for(var i=0; i<itemSecondaryZone.length; i++) {
		var elem = itemSecondaryZone[i];
		menuBar.addItemToSecondaryZone(
			createMenuBarItem(elem));
	}

	return menuBar;
}

function createDimen(dimenElem) {
	return new Dimensions (
		getNodeContentBySelector(dimenElem, "> .width", undefined),
		getNodeContentBySelector(dimenElem, "> .height", undefined),
		getNodeContentBySelector(dimenElem, "> .marginup", undefined),
		getNodeContentBySelector(dimenElem, "> .margindown", undefined),
		getNodeContentBySelector(dimenElem, "> .marginleft", undefined),
		getNodeContentBySelector(dimenElem, "> .marginright", undefined));
}

function createSectionBlock(blockElem) {
	var dimenElem = blockElem.querySelector("> .dimen");
	return new SectionBlock (
		getNodeContentBySelector(blockElem, "> .title", undefined),
		getNodeContentBySelector(blockElem, "> .link", undefined),
		getNodeContentBySelector(blockElem, "> .icon", undefined),
		getNodeContentBySelector(blockElem, "> .text", undefined),
		getNodeContentBySelector(blockElem, "> .opacity", undefined),
		(dimenElem==null)?undefined:createDimen(dimenElem),
		getNodeContentBySelector(blockElem, "> .elementId", undefined),
		getNodeContentBySelector(blockElem, "> .elementClass", undefined));
}

function createSection(sectionElem) {
	var dimenElem = sectionElem.querySelector("> .dimen");
	var section = new Section(
		getNodeContentBySelector(sectionElem, "> .title", undefined),
		getNodeContentBySelector(sectionElem, "> .link", undefined),
		getNodeContentBySelector(sectionElem, "> .opacity", undefined),
		(dimenElem==null)?undefined:createDimen(dimenElem),
		getNodeContentBySelector(sectionElem, "> .elementId", undefined),
		getNodeContentBySelector(sectionElem, "> .elementClass", undefined));

	var blockElems = sectionElem.querySelectorAll("> .sectionBlock");
	for(var i=0; i<blockElems.length; i++) {
		var blockElem = blockElems[i];
		section.addBlock(
			createSectionBlock(blockElem));
	}
	return section;
}

function createSectionList(sectionElems) {
	var sections = [];
	for(var i=0; i<sectionElems.length; i++) {
		var elem = sectionElems[i];
		sections.push(createSection(elem));
	}
	return sections;
}

function createPage(rootElem) {
	var menuBarElem = rootElem.querySelector(".menuBar");
    var sectionElems = rootElem.querySelectorAll(".section");
    var footerSectionElem = rootElem.querySelector(".footerSection");

    return new Page(
    	getNodeContentBySelector(rootElem, ".title", undefined),
    	(menuBarElem==null)?undefined:createMenuBar(menuBarElem),
    	(sectionElems==null)?undefined:createSectionList(sectionElems),
    	(footerSectionElem==null)?undefined:createSection(footerSectionElem));
}

/**
* onSuccess (Page)
* onFail ()
*/
var loadMenuBar = function (filename, onSuccess, onFail) {
	fs.readFile(filename, 'utf8', function (err, data) {
		if(err) {
            console.log(err);
            onFail();
            return;
        }

        var rootElem = parser.parse(data);
        var menuBarElem = rootElem.querySelector(".menuBar");
        onSuccess(
        	createMenuBar(menuBarElem));
    })
}

/**
* onSuccess (Page)
* onFail ()
*/
var loadPage = function (filename, menubarfilename, onSuccess, onFail) {
	fs.readFile(filename, 'utf8', function (err, data) {
		if(err) {
            console.log(err);
            onFail();
            return;
        }

        var rootElem = parser.parse(data);

        if(menubarfilename != undefined) {
        	loadMenuBar(menubarfilename,
        		function (menuBar) {
        			var page = createPage(rootElem);
        			if(page.menuBar == undefined) {
        				page.menuBar = menuBar;
        			}
        			onSuccess(page);
	        	},
	        	function () {
	        		onFail();
	        	}
        	)
        }
        else {
        	onSuccess(
        		createPage(rootElem));
        }
	});
}

module.exports = {
	Section, SectionBlock, Dimensions,
	MenuBar, MenuBarItem,
	Page,
	loadMenuBar, loadPage
};
