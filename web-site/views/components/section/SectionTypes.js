/**
* Every parameter represent a pourcentage.
* Set a value lower than 0 to diseable a parameter.
*/
function Dimensions (width, height, marginup, margindown, marginleft, marginright) {
	this.width = width;
	this.height = height;
	this.marginup = marginup;
	this.margindown = margindown;
	this.marginleft = marginleft;
	this.marginright = marginright;
}

/**
* @param {string} title
* @param {string} link
* @param {string} icon
* @param {string} text
* @param {number} opacity [0.0-1.0]
* @param {Dimensions} dimen
* @param {string} elementId
* @param {string} elementClass
*/
function SectionBlock (
	title, link, icon, text,
	opacity, dimen,
	elementId, elementClass) {
	
	this.elementId = elementId;
	this.elementClass = elementClass;

	this.title = title;
	this.link = link;
	this.icon = icon;
	this.text = text;
	
	this.dimen = dimen;
	this.opacity = opacity;
}

/**
* @param {string} title
* @param {string} link
* @param {number} opacity [0.0-1.0]
* @param {Dimensions} dimen
* @param {string} elementId
* @param {string} elementClass
*/
function Section (title, link, opacity, dimen, elementId, elementClass) {
	this.elementId = elementId;
	this.elementClass = elementClass;

	this.title = title;
	this.link = link;
	this.opacity = opacity;
	this.dimen = dimen;

	this.blocks = undefined;

	this.addBlock = function (block) {
		if(this.blocks == undefined) {
			this.blocks = [];
		}
		this.blocks.push(block);
	}
}

module.exports = {Section, SectionBlock, Dimensions};
